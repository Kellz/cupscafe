<a href="http://fvcproductions.com"><img src="https://media3.giphy.com/media/687qS11pXwjCM/giphy.gif" title="FVCproductions" alt="FVCproductions"></a>
# CupsCafe

Advanced Programming Group Project
### Members: 

- Team Leader: Kelleshia Kinlocke ID 1603337
- Integration Leader: Rohan Bedward ID 1602960  and David Williams ID 1601916

## Description

Cups is a local coffee shop that provides a relaxing getaway in the middle of the city for the disabled community. They are also a wonderful example of a Social Enterprise Boost Initiative (https://www.micaf.gov.jm/msme-initiatives/social-enterprise-boost-initiative-sebi) similar to DeafCan coffee (https://www.deafcancoffee.com/ | https://youtu.be/gGYq0ASoEPM ). Kat, the manager, has been encouraged by her mentor to establish another store at 95 Moolean Avenue in the heart of Montego Bay. Kat would like to encourage an empowering environment through self service. Your consulting team providing pro bono services has considered to incorporate Artificial Intelligence through Computer Vision and Speech Processing to accomplish this. The touch-screen self service kiosk will allow customers to order their favourite treats and verify using their Digital Id.

 
### How to Run Project
- step 1 - run the following commands in terminal
composer install 
&
composer update
- step 2 - if the commands in step 1 does not work please do step and run "composer install" command after
rm -rf vendor

- step 3 - to start laravel
php artisan serve



### Installing

-
* [Laravel Setup Tutorial](https://www.youtube.com/watch?v=H3uRXvwXz1o&list=PLillGF-RfqbYhQsN5WMXy6VsDMKGadrJ-&index=2) 

## Running the tests

- 

## Deployment

-

## Built With

* [Laravel](https://laravel.com/docs/6.x) - The web framework used
* [Composer](https://getcomposer.org/doc/) - application-level package manager for the PHP
* [phpMyAdmin](https://www.phpmyadmin.net/) - Database
* [Visual Studio Code](https://visualstudio.microsoft.com/) -  Code Editor


## Contributing

* [Parental by Tighten](https://github.com/calebporzio/parental) - Parental is a Laravel package that brings STI (Single Table Inheritance) capabilities to Eloquent.
* [Dropzone.js](https://www.dropzonejs.com/)
* [p5.js](https://p5js.org/)
* [ml5.js](https://ml5js.org/)
## Versioning

-

## Acknowledgments

* [Laravel Tutorial](https://www.youtube.com/watch?v=H3uRXvwXz1o&list=PLillGF-RfqbYhQsN5WMXy6VsDMKGadrJ-&index=2) 

