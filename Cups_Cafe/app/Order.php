<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public $timestamps = false;
    public $table = 'order';

	protected $dates = [
		 'time_of_placement'
    ];

    protected $fillable = [
        'cust_id','cust_name','total','balance'
    ];
    
    public function customer(){
        return $this->belongsTo('App\Customer');
    }

    public function items(){
		return $this->belongsToMany('App\items')->withPivot('quantity');
    }
}
