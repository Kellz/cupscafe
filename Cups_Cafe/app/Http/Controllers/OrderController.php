<?php

namespace App\Http\Controllers;

use DB;
use App\Order;
use App\Items;
use App\order_items;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Gloudemans\Shoppingcart\Facades\Cart;
use Session;
class OrderController extends Controller
{
   public function __construct()
    {
        $this->middleware('auth:customer');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

     
    public function index()
    {   
        Session::forget('attempts');
        return view('order.ordermenu',
            [
                "menus" => Items::orderBy("created_at", "DESC")->get()
            ]
        );
    }

    public function search(Request $request){
        $search = $request -> get('search');
        $file=$request-> get('upload');
        foreach (Items::orderBy("created_at", "DESC")->get() as $item)
            if($item->name == $search || $item->asl_item == $file || $item->item_audio == $file)
                {
                    Session::flash("success", "Menu Item Found");
                    return view('order.search-result',['menu' => $item]);
                }

            Session::flash("error", "Menu Item Not Found");
            return view('order.search-result',['menu' => 'null']);   
        
    } 


    public function store(Request $request)
    {
        
        return Redirect::back();

    }
  
    
    /**
     * 
     * @param \Illuminate\Http\Request  $request
     * @param int $id
     * @param \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
        $order = order_items::findOrFail($request->id);
        $order->update(['status' => 'Complete']);
        return back();
    }

    /**
     * 
     * @param \Illuminate\Http\Request  $request
     * @param int $id
     * @param \Illuminate\Http\Response
     */

    public function cancel(Request $request, $id)
    {
        $order = order_items::findOrFail($request->id);
        $order->update(['status' => 'Cancelled']);
        return back();
    }

}