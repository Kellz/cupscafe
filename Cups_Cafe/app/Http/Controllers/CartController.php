<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use App\order_items;
use App\Order;
use App\Customer;
use Illuminate\Support\Facades\Redirect;
use Session;
class CartController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:customer');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Cart::content();
        return view("order.cart")->with('items',$items); 
    }

        public function logout() {
        session::flush();
        return redirect()->route('cust.login');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $duplicates = Cart::search(function ($cartItem, $rowId) use ($request){
            return $cartItem->id == $request->id;
        });
      
        Cart::add($request->id,$request->item_name,$request->qty,$request->cost)->associate('App\order_items');
        Session::flash("success", "Item Added To Cart Successfully");
        return Redirect::back();
    }

    public function confirm(Request $request){
        $customer = false;
        $val = $request->attempts;
        $val++;

        if($val == 3){
            session::flush();
            return redirect()->route('cust.login');
        }else{
            Session::put('attempts', $val);
        }
        
        if(Session::get('fname') == $request->fname && Session::get('lname') == $request->lname){
           $customer = Customer::where('fname', $request->fname)
            ->where('lname', $request->lname)
            ->first();
        }
            $id = $request->digital_id;
            if($id != 1){
            if ($customer && Hash::check($request->password, $customer->password)) {
                Session::forget('attempts');
                $total= Cart::total();

                if($customer->quota>$total){
                    
                    $updatedquota = $customer->quota - $total;
                    
                    $order = Order::create([
                        'cust_id' => $customer->cust_id,
                        'cust_name'=>  $customer->fname.' '.$customer->lname,
                        'total'=> $total,
                        'balance'=> $updatedquota,
                    ]);
           
                     foreach (Cart::content() as $item){
                        order_items::Create([
                             'order_id' => $order->id,
                             'item_id'=>$item->id,
                             'quantity'=> $item->qty,
                             'cost'=>$item->price,
                        ]);
                     }
                     $customer->quota = $updatedquota;
                     $customer->save();

                    Cart::instance('default')->destroy();
                    Session::flash("success", "Order Has Been Placed Successfully");
                    return redirect()->intended(route('order.store'));
                }
                    else
                        Session::flash("error", "Insufficient Points     Available: $ $customer->quota");
            }
            else

                Session::flash("error", "Invalid Credentials");
                return Redirect::back();
    }else{
        Session::put('clname', $request->lname);
        Session::put('cfname', $request->fname);
        return redirect()->intended(route('order.confirm.getPose'));
        //return doPose();
    }
    }

     public function getLoginPose()
    {
        return view('order.poseConfirmation');
    }
    
    public function doPose(){
        return view('order.poseConfirmation');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }


    public function remove(Request $request)
    {
        
        Cart::remove($request->id);
      
        Session::flash("success", "Item Removed successfully");
        return Redirect::back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        Cart::destroy();

        Session::flash("success", "Cart Has been Emptied");

        return Redirect::back();
    }
}