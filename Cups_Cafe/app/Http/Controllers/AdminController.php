<?php

namespace App\Http\Controllers;
use App\Items;
use App\Admin;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Session;
class AdminController extends Controller
{
   public function __construct()
    {
        $this->middleware('auth:admin');
    }
    

    public function logout() {
        Session::flush();
        return Redirect('admin/login');
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('admin.adminmenu',
            [
                "menus" => Items::orderBy("created_at", "DESC")->get()
            ]
        );
    }

    public function showForm(){
        return view("admin.additem", []);
    }

    public function create(Request $request){
        $this->validate($request, [
            "unit_quantity" => "required|numeric",
            "cost" => "required|numeric",
            "name" => "required|max:255|min:3",
            "category" => "required",
            "item_photo" => "image",
            "asl_item" => "image",
            "item_audio" => "required"
        ]);
        //Insert Data to data base
        $item = new Items();
        $item->emp_id = Auth::guard('admin')->user()->emp_id;
        $item->unit_quantity = $request->unit_quantity;
        $item->cost = $request->cost;
        $item->name = $request->name;
        $item->category = $request->category;
        $item->item_photo = $request->item_photo;
        $item->asl_item = $request->asl_item;
        $item->item_audio = $request->item_audio;
       
        

        

        //item image
        if($item->item_photo != null && !empty($item->item_photo)){
            $fileName = $item->item_photo->getClientOriginalName();
            $extension = $item->item_photo->getClientOriginalExtension();
            if($this->imageFileTypeValidator(strtolower($extension)) == false){
                Session::flash('error', 'Please upload an image. File you\'re trying to upload is not supported');
                return redirect()->back();
            }else{
                $item->item_photo->storeAs('public/item_photo', $fileName);
                $item->item_photo = $fileName;
            }
        }
        //ASL for item
        if($item->asl_item != null && !empty($item->asl_item)){
            $fileName = $item->asl_item->getClientOriginalName();
            $extension = $item->asl_item->getClientOriginalExtension();
            if($this->imageFileTypeValidator(strtolower($extension)) == false){
                Session::flash('error', 'Please upload an image. File you\'re trying to upload is not supported');
                return redirect()->back();
            }else{
                $item->asl_item->storeAs('public/asl_item', $fileName);
                $item->asl_item = $fileName;
            }
        }
        // verifying for audio
        if($item->item_audio != null && !empty($item->item_audio)){
            $fileName = $item->item_audio->getClientOriginalName();
            $extension = $item->item_audio->getClientOriginalExtension();
            if($this->audioFileTypeValidator(strtolower($extension)) == false){
                Session::flash('error', 'Please upload an audio. File you\'re trying to upload is not supported');
                return redirect()->back();
            }else{
                $item->item_audio->storeAs('public/item_audio', $fileName);
                $item->item_audio = $fileName;
            }
        }

        if($item->save()){
            Session::flash("success", "Record was created successfully");
        }else{
            Session::flash(["error", "Fail to save record. Please try again"]);
        }
        return Redirect::back();

    }
    public function audioFileTypeValidator($extention){
        $supported_formats = array(
            'mp3',
            'wav',
            'm4a',
            'mp4'
        );

        if(in_array($extention, $supported_formats)){
            return true;
        }
        return false;

    }


    public function imageFileTypeValidator($extention){
        $supported_formats = array(
            'gif',
            'jpg',
            'jpeg',
            'png'
        );

        if(in_array($extention, $supported_formats)){
            return true;
        }
        return false;

    }

    public function show($id){
        $item = Items::find($id);
        if($item != null && !empty($item)){
            //item found
            return view("admin.item", ["item" => $item]);
        }else{
            // redirect to not found page
        }
    }
   public function destroy($id){
    $item = Items::find($id); 
    if($id){
        if($item->delete($id)){
            return Redirect::to('/admin')->with('message', 'Success: Deleted!');
        }else{
            Session::flash('message', 'Error: Not Deleted!');
        }
    }
    
    }
    public function edit($id)
    {
        try{
            //Find the item object from model if it exists
            $item= Items::findOrFail($id);
            //Redirect to edit item form with the item info found above.
            return view('add',['item'=>$item]);
        }
        catch(ModelNotFoundException $err){
            //redirect to your error page
            return Redirect::to('/admin')->with('message', 'Error, Try Again Later');
        }
    } 
    public function update(Request $request, $id)
    {
        try{
            //Find the item object from model if it exists
            $item= Items::Where('item_id', $request->item_id) ->first();
            //Set item object attributes 
            $item->unit_quantity = $request['unit_quantity'];
            $item->cost = $request['cost'];
            //Save/update user.
            $item->save();
            return Redirect::to('/admin')->with('message', 'Item Updated Successfully');
            
        }
        catch(ModelNotFoundException $err){
            //Show error page
        }       
    }      

}