<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use config\logging;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Response;
use App\blindCust;
use App\regCust;
use App\deafCust;
use App\Customer;
use Session;
use db;
class CustomerLoginController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:customer');
    }

    protected function getCredentials(Request $request)
    {
        return $request->only('fname', 'lname', 'password');
    }

    public function custLoginForm()
    {   
        return view('auth.clientLogin');
    }

    public function custLogin(Request $request){
        $value = $request->digital_id;
        Session::flush();
        if ($value == 0) {
            $this->validate($request, [
                'fname' => 'required',
                'lname' => 'required',
                'password' => 'required'
            ]);   
            
            $customer = Customer::where('fname', strtolower($request->fname))
            ->where('lname', strtolower($request->lname))
            ->first();

            if ($customer && Hash::check($request->password, $customer->password)) {
                Auth::guard('customer')->loginUsingId($customer->cust_id);
                $request->session()->put('fname', strtolower($request->fname));
                $request->session()->put('lname', strtolower($request->lname));
                $request->session()->put('password', Hash::make($request->password));
                return redirect()->intended(route('order.store'));
            }
            return redirect()->back();
        }else if ($value == 1){
            $this->validate($request, [
            'fname' => 'required|min:3|max:10',
            'lname' => 'required|min:3|max:10',
            ]);
            $request->session()->put('fname', strtolower($request->fname));
            $request->session()->put('lname', strtolower($request->lname));
            $request->session()->put('password', Hash::make(strtolower($request->fname).''.strtolower($request->lname)));
            $request->session()->put('poseMode', 'get');
            return redirect()->route('cust.login.getLoginPose');
        }else if ($value == 2){
            $this->validate($request, [
                'fname' => 'required',
                'lname' => 'required',
                'password' => 'required'
            ]);  
            
                
            $customer = Customer::where('fname', strtolower($request->fname))
            ->where('lname', strtolower($request->lname))
            ->first();
            if ($customer && Hash::check($request->password, $customer->password)) {
                Auth::guard('customer')->loginUsingId($customer->cust_id);
                $request->session()->put('fname', strtolower($request->fname));
                $request->session()->put('lname', strtolower($request->lname));
                $request->session()->put('password', Hash::make($request->password));
                return redirect()->intended(route('order.store'));
            }
            return redirect()->back();
        }
    }

     public function getLoginPose()
    {
        return view('auth.loginPose');
    }

    public function loginPose(Request $request)
    {
        $fname = strtolower($request->first_name);
        $lname = strtolower($request->last_name);
        $password = Session::get('fname') ."". Session::get('lname');
        $customer = Customer::where('fname', $fname)
            ->where('lname',  $lname)
            ->first();
            if ($customer && Hash::check($password, $customer->password)) {
                Auth::guard('customer')->loginUsingId($customer->cust_id);
                return redirect()->intended(route('order.store'));
            }
            return redirect()->back();
    }

     public function getSetPose()
    {
        return view('auth.setPose');
    }

    public function signupForm()
    {
        return view('auth.clientRegister');
    }
    
    public function signup(Request $request){
        
        $value = $request->digital_id;
        
        if ($value == 0) {

            $customer = Customer::where('fname', $request->fname)
            ->where('lname', $request->lname)
            ->first();
            if ($customer) {
               return redirect('cust/signup');
            }else{
                  $this->validate($request, [
            'fname' => 'required|min:3|max:10',
            'lname' => 'required|min:3|max:10',
            'password' => 'required|confirmed|min:6',
            ]);
            
            $hashPassword = Hash::make($request->password);
            $regCust = new Customer();
            $regCust->fname = strtolower($request->fname);
            $regCust->lname = strtolower($request->lname);
            $regCust->quota = '500';
            $regCust->cust_type = 'regCust';
            $regCust->password = $hashPassword;
            $regCust->save();
            return redirect('cust/login');

            }
        } else if ($value == 1) {
            $customer = Customer::where('fname', $request->fname)
            ->where('lname', $request->lname)
            ->first();
            if ($customer) {
               return redirect('cust/signup');
            }else{
            $this->validate($request, [
            'fname' => 'required|min:3|max:10',
            'lname' => 'required|min:3|max:10',
            ]);
            $request->session()->put('fname', strtolower($request->fname));
            $request->session()->put('lname', strtolower($request->lname));
            $request->session()->put('poseMode', 'set');
            return redirect()->route('cust.signup.getSetPose');
            }
        }else if ($value == 2){
            $customer = Customer::where('fname', $request->fname)
            ->where('lname', $request->lname)
            ->first();
            if ($customer) {
               return redirect('cust/signup');
            }else{
            $this->validate($request, [
            'fname' => 'required|min:3|max:10',
            'lname' => 'required|min:3|max:10',
            'password' => 'required|confirmed|min:6',
            ]);
            $hashPassword = Hash::make($request->password);
            $blindCust = new Customer();
            $blindCust->fname = strtolower($request->fname);
            $blindCust->lname = strtolower($request->lname);
            $blindCust->quota = '500';
            $blindCust->cust_type = 'blindCust';
            $blindCust->password = $hashPassword;
            $blindCust->save();
            return redirect('cust/login');
            }
        }
    }

  public function setPose(Request $request){
    $string = Session::get('fname') ."". Session::get('lname');
    $hashPassword = Hash::make(preg_replace('/\s+/', '', $string));
    $deafCust = new Customer();
    $deafCust->fname =  Session::get('fname');
    $deafCust->lname =  Session::get('lname');
    $deafCust->quota = '500';
    $deafCust->cust_type = 'deafCust';
    $deafCust->password = $hashPassword;
    $deafCust->save();
    return redirect('cust/login');
  }
         public function setPoseUpload(Request $request){
    if($request->hasFile('file'))
    	{
    		$modelFile = $request->file('file');
    		$modelName = $modelFile->getClientOriginalName();
    		$modelFile->move(public_path('model'), $modelName);
    	}
    	return response()->json(['Status'=>true, 'Message'=>'Files Uploaded.']);
   }
}







