<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Session\Store;
use App\Admin;
use db;
use config\logging;
use Session;
class AdminLoginController extends Controller
{
   
     public function __construct()
    {
        $this->middleware('guest:admin');
    }
    
    public function adminLoginForm()
    {
        return view('auth.adminLogin');
    }
    public function adminLogin(Request $request){
        //flush the session to logout any user currently logged in
        Session::flush();

        $this->validate($request,[
            'email' => 'required|email',
            'password' => 'required'
        ]);
        $admin = Admin::where('email', $request->email)->first();

            if ($admin && Hash::check($request->password, $admin->password)) {
                Auth::guard('admin')->loginUsingId($admin->emp_id);
                $request->session()->put('name', $request->emp_name);
                $request->session()->put('email', $request->email);
                $request->session()->put('password', Hash::make($request->password));
                return redirect()->intended(route('admin.index'));
            }
            return redirect()->back();
    }
    
}