<?php

namespace App\Http\Controllers;


use App\Charts\SalesChart;
use Illuminate\Http\Request;
use App\Items;
use App\order_items;
use Charts;

class SalesChartController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function invoke()
    {        
        $items= order_items::orderBy("created_at", "DESC")->get();

        //dd($check);
        $results= Items::orderBy("created_at", "DESC")->get();
        foreach ($results as $result)
        {
            $result->unit_quantity=0;
            $result->cost = 0;
        }

        foreach($items as $item){
            $update = $results->find($item->item_id);
            
            $update->unit_quantity+=$item->quantity;
            
            $update->cost+=$item->quantity * $item->cost ;
            
        }
        
        $names =$results->pluck('name')->values();
        $sales =$results->pluck('cost')->values();

         $salesChart = new SalesChart;        
         $salesChart->labels($names);
         $salesChart->dataset('Sales per Items', 'line', $sales)
            ->color("rgb(222,184,135)")
            ->backgroundcolor("rgb(222,184,135)");          
            
        return view('admin/salechart', compact('salesChart'));
                    // [ 'usersChart' => $usersChart ]
        
    }

    
}
