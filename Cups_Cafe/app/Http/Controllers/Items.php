<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class Items extends Controller
{
     public function __construct()
    {
        $this->middleware('guest:customer');
    }

    public function Items()
    {
    $items = DB::table('Items')->get();

    return view('home')->with('items',$items);
    }   
}
