<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Items extends Model
{
    const ITEM_DIRECTORY = "items";
    protected $table = 'items';
    protected $primaryKey = 'item_id';


    protected $fillable = [
       'emp_id', 'unit_quantity','cost','name','category','item_photo'];

    public function admin(){
        return $this->belongsTo('App\Admin');
    }
    
}
