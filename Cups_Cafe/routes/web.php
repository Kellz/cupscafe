<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','items@Items')->name('home');

Auth::routes();

// A D M I N  R O U T E S 
Route::prefix('admin')->group( function() {
    Route::get('/login','Auth\AdminLoginController@adminLoginForm')->name('admin.login');
    Route::post('/login','Auth\AdminLoginController@adminLogin')->name('admin.login.submit');
    Route::get('/','AdminController@index')->name('admin.index');
    Route::post('delete/{id}','AdminController@destroy')->name('admin.destroy');
    Route::post('update/{id}','AdminController@update')->name('admin.update');
    Route::get('/salechart', 'SalesChartController@invoke')->name('admin.salechart');
   
    Route::get('/create', [
        "uses" => "AdminController@showForm",
        "as" => "admin.add.item.form"
    ]);

    Route::post('/create/record', [
        "uses" => "AdminController@create",
        "as" => "admin.create.item"
    ]);
    Route::get('/logout','AdminController@logout')->name('admin.logout');
});

//C U S T O M E R  L O G I N  &  S I G N U P
Route::prefix('cust')->group( function(){
Route::get('/login','Auth\CustomerLoginController@custLoginForm')->name('cust.login');
Route::post('/login','Auth\CustomerLoginController@custLogin')->name('cust.login.submit');
Route::get('/login/getLoginPose/','Auth\CustomerLoginController@getLoginPose')->name('cust.login.getLoginPose');
Route::post('/login/loginPose','Auth\CustomerLoginController@loginPose')->name('cust.login.loginPose');

Route::get('/signup','Auth\CustomerLoginController@signupForm')->name('cust.signup');
Route::post('/signup','Auth\CustomerLoginController@signup')->name('cust.signup.submit');
Route::get('/signup/setPose/','Auth\CustomerLoginController@getSetPose')->name('cust.signup.getSetPose');
Route::post('/signup/setPose','Auth\CustomerLoginController@setPose')->name('cust.signup.setPose');
Route::post('/signup/setPoseUpload','Auth\CustomerLoginController@setPoseUpload')->name('cust.signup.setPoseUpload');

//O R D E R  R O U T E S
//ordermenu
Route::get('/ordermenu','OrderController@index')->name('order.index');

//Checkout Route
Route::patch('/cart/update/{id}','OrderController@update')->name('order.update');
//Search Route
Route::get('/search','OrderController@search')->name('order.search');
Route::patch('/cart/cancel/{id}','OrderController@cancel')->name('order.cancel');
//Cart Routes
Route::get('/cart','CartController@index')->name('order.cart');
Route::post('/check-out','CartController@confirm')->name('order.confirm');
Route::get('/getPoseConfirmation','CartController@doPose')->name('order.confirm.getPose');
Route::post('/poseConfirmation','CartController@doPose')->name('order.confirm.pose');
Route::post('/ordermenu','CartController@store')->name('order.store');
Route::post('/cust/cart','CartController@destroy')->name('order.destroy');
Route::post('/cart','CartController@remove')->name('order.remove');
Route::get('/logout','CartController@logout')->name('cust.logout');

});
