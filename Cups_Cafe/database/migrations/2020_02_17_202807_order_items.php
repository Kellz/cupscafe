<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class OrderItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
          public function up()
    {
        Schema::create('order_items', function (Blueprint $table) {
            
            $table->bigInteger('order_id')->unsigned();
            $table->bigInteger('item_id')->unsigned();
             $table->float('cost');
            $table->bigInteger('quantity')->unsigned();
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('order_id')->references('order_id')->on('order')->onDelete('cascade');                    
            $table->foreign('item_id')->references('item_id')->on('items')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_items');
    }
}
