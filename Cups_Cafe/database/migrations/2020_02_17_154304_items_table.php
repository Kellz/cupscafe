<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->bigIncrements('item_id');
            $table->unsignedBigInteger('emp_id');
            $table->bigInteger('unit_quantity');            
            $table->float('cost');
            $table->string('name');
            $table->string('category');
            $table->string('item_photo');
            $table->string('asl_item');
            $table->string('item_audio');

            $table->foreign('emp_id')->references('emp_id')->on('admins')->onDelete('cascade');
            $table->rememberToken();
            $table->timestamps();
        });  
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
