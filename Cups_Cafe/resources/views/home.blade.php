@extends('layouts.Layout')

@section('content')
    
    <section class="home-slider owl-carousel">

      <div class="slider-item" style="background-image: url(images/bg_3.jpg);" data-stellar-background-ratio="0.5">
      	<div class="overlay"></div>
        <div class="container">
          <div class="row slider-text justify-content-center align-items-center">

            <div class="col-md-7 col-sm-12 text-center ftco-animate">
            	<h1 class="mb-3 mt-5 bread">Welcome</h1>
            </div>

          </div>
        </div>
      </div>
	</section>
	<section class="ftco-menu mb-5 pb-5">
		<div class="container">
			<div class="row justify-content-center mb-5">
			<div class="col-md-7 heading-section text-center ftco-animate">
				<span class="subheading">C.U.P.S </span>
				<h2 class="mb-4">Our Establishment</h2>
				<p>C.U.P.S Cafe exists to affirm young people in their identity as a Deaf person and as someone made in the image of God. Success is defined as young men and women accepting themselves, believing in their inherent gifts and talents, taking responsibility for their future and becoming a leader in their families, communities and careers. We are an outreach of HarvestCall Jamaica, a not-for-profit organization founded to obey God and love others.</p>
			</div>
		</div>
		
	</section>
    <section class="ftco-menu mb-5 pb-5">
    	<div class="container">
			<div class="row justify-content-center mb-5">
			<div class="col-md-7 heading-section text-center ftco-animate">
				<span class="subheading">Menu</span>
				<h2 class="mb-4">Products</h2>
				<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
			</div>
		</div>

    		<div class="row d-md-flex">
	    		<div class="col-lg-12 ftco-animate p-md-5">
		    		<div class="row">
		          <div class="col-md-12 nav-link-wrap mb-5">
		            <div class="nav ftco-animate nav-pills justify-content-center" id="v-pills-tab" role="tablist" aria-orientation="vertical">
		            	<a class="nav-link active" id="v-pills-0-tab" data-toggle="pill" href="#v-pills-0" role="tab" aria-controls="v-pills-0" aria-selected="true">Beverage</a>

		              <a class="nav-link" id="v-pills-1-tab" data-toggle="pill" href="#v-pills-1" role="tab" aria-controls="v-pills-1" aria-selected="false">Snack</a>

		              <a class="nav-link" id="v-pills-2-tab" data-toggle="pill" href="#v-pills-2" role="tab" aria-controls="v-pills-2" aria-selected="false">Daily Surprise</a>
		            </div>
		          </div>
		          <div class="col-md-12 d-flex align-items-center">
		            
		            <div class="tab-content ftco-animate" id="v-pills-tabContent">
					
		              <div class="tab-pane fade show active" id="v-pills-0" role="tabpanel" aria-labelledby="v-pills-0-tab">
		              	<div class="row">
						<div class="col-md-4 text-center">
							@foreach ($items as $item)
							 @if($item->category == "beverage")
							<div class="menu-wrap">
                   				 <img class="menu-img img mb-4 col-md-12" src="storage/item_photo/{{$item->item_photo}}" />
								<div class="text text-center pt-4">
									<h3><a>{{$item->name}}</a></h3>
									<p class="price"><span>${{$item->cost}}</span></p>
									<p><a href="{{ route('cust.login') }}" class="btn btn-primary btn-outline-primary">Login Then Add to Cart</a></p>
								</div>
							</div>
							 @endif
							 @endforeach
						</div>
		             </div>
					 </div>

		              <div class="tab-pane fade" id="v-pills-1" role="tabpanel" aria-labelledby="v-pills-1-tab">
		                <div class="row">
		            	  <div class="col-md-4 text-center">
							@foreach ($items as $item)
							 @if($item->category == "snack")
							<<div class="menu-wrap">
                   				 <img class="menu-img img mb-4 col-md-12" src="storage/item_photo/{{$item->item_photo}}" />
								<div class="text text-center pt-4">
									<h3><a href="product-single.html">{{$item->name}}</a></h3>
									<p class="price"><span>${{$item->cost}}</span></p>
									<p><a href="{{ route('cust.login') }}" class="btn btn-primary btn-outline-primary">Login Then Add to Cart</a></p>
								</div>
							</div>
							 @endif
							 @endforeach
						</div>
		              	</div>
		              </div>

		              <div class="tab-pane fade" id="v-pills-2" role="tabpanel" aria-labelledby="v-pills-2-tab">
		                <div class="row">
		              		<div class="col-md-4 text-center">
							@foreach ($items as $item)
							@if($item->category == "daily-surprise")
							<div class="menu-entry" style="width: 200px;">
								<div class="menu-wrap">
                   				 <img class="menu-img img mb-4 col-md-12" src="storage/item_photo/{{$item->item_photo}}" />
								<div class="text text-center pt-4">
									<h3><a href="product-single.html">{{$item->name}}</a></h3>
									<p class="price"><span>${{$item->cost}}</span></p>
									<p><a href="{{ route('cust.login') }}" class="btn btn-primary btn-outline-primary">Login Then Add to Cart</a></p>
									
								</div>
							</div>
							@endif
							@endforeach
						</div>
		              	
		              	</div>
		              </div>
		            </div>
		          </div>
		        </div>
		      </div>
		    </div>
    	</div>
    </section>

    <section class="ftco-counter ftco-bg-dark img" id="section-counter" style="background-image: url(images/bg_2.jpg);" data-stellar-background-ratio="0.5">
			<div class="overlay"></div>
      <div class="container">
        <div class="row justify-content-center">
        	<div class="col-md-10">
        		<div class="row">
		          <div class="col-md-6 col-lg-3 d-flex justify-content-center counter-wrap ftco-animate">
		            <div class="block-18 text-center">
		              <div class="text">
		              	<div class="icon"><span class="flaticon-coffee-cup"></span></div>
		              	<strong class="number" data-number="1">0</strong>
		              	<span>Coffee Branches</span>
		              </div>
		            </div>
		          </div>
		          <div class="col-md-6 col-lg-3 d-flex justify-content-center counter-wrap ftco-animate">
		            <div class="block-18 text-center">
		              <div class="text">
		              	<div class="icon"><span class="flaticon-coffee-cup"></span></div>
		              	<strong class="number" data-number="3">0</strong>
		              	<span>Number of Awards</span>
		              </div>
		            </div>
		          </div>
		          <div class="col-md-6 col-lg-3 d-flex justify-content-center counter-wrap ftco-animate">
		            <div class="block-18 text-center">
		              <div class="text">
		              	<div class="icon"><span class="flaticon-coffee-cup"></span></div>
		              	<strong class="number" data-number="100">0</strong>
		              	<span>Happy Customer</span>
		              </div>
		            </div>
		          </div>
		          <div class="col-md-6 col-lg-3 d-flex justify-content-center counter-wrap ftco-animate">
		            <div class="block-18 text-center">
		              <div class="text">
		              	<div class="icon"><span class="flaticon-coffee-cup"></span></div>
		              	<strong class="number" data-number="3">0</strong>
		              	<span>Staff</span>
		              </div>
		            </div>
		          </div>
		        </div>
		      </div>
        </div>
      </div>
    </section>

@endsection 