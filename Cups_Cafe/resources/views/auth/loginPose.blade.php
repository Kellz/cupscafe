@extends('layouts.Layout')

@section('content')

<section class="main">
    <script src="{{ asset('functionality/pose.js') }}"></script>
    <div class="login_div" style="background-image: url('{{ asset('images/bg_3.jpg')}}'); height: 1100px; padding: 30px;" >
    
    <div class="center" id="sketch" style="display: block; width:600px; margin: auto; padding: 60px 0px 5px 0px;">
    </div>

    <form id = "poseLoginForm" action="{{ route('cust.login.loginPose') }}" method="POST">
        @csrf
        <input type="hidden" name="first_name" value="{{ Session::get('fname')}}">
        <input type="hidden" name="last_name" value="{{ Session::get('lname')}}">
    </form>
    </div>
</section>
<script> 
    if (!'{{Session::has('fname')}}')
    {
        alert("Please enter your name before setting pose");
        window.location.href = "{{route('cust.login')}}";
    }

    var str1 = '{{ Session::get('fname')}}';
    var str2 = '{{ Session::get('lname')}}';
    var poseMode = '{{ Session::get('poseMode')}}'; 
    var poseID = str1.concat(""+str2);
    poseID = poseID.replace(/\s+/g, '');    
    const public_path1 = '/model/'+poseID+'.json';
    const public_path2 = '/model/'+poseID+'_meta.json';
    const public_path3 = '/model/'+poseID+'.weights.bin';
    var PostUri = '{{ route('cust.login.loginPose') }}';
    var main = '{{route('cust.login')}}';
 
    setTimeout(function () {
            console.log('pose not found');
            alert('sorry your pose was not found or the page timed out please try again');
            window.location.href = main;
        }, 30000);
   
</script> 
@endsection










