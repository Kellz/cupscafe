@extends('layouts.Layout')

@section('content')

<section class="main">
    <script src="{{ asset('functionality/pose.js') }}"></script>
    <div class="login_div" style="background-image: url('{{ asset('images/bg_3.jpg')}}'); height: 1100px; padding: 30px;" >
    
    <div class="center" id="sketch" style="display: block; width:600px; margin: auto; padding: 60px 0px 5px 0px;">
        <div style="margin-right: 0%;">
    <div id="user" data-id="1234567890" data-user="<?php Session::get('fname'); ?>" ></div>
 
 
            <p style="color:red; font-weight: bold;">Instruction: </p>
            <p style="color:red; font-weight: bold;">Step: 1 set pose that you don't want by clicking 'wrong pose' </p>
            <p style="color:red; font-weight: bold;">Step: 2 set pose that you want by clicking 'collect' </p>
            <p style="color:red; font-weight: bold;">Step: 3 train model by clicking 'train' </p>
        </div>
        <div style="background-color: red; margin-right: auto; width: 300px; padding: 10px;">Wait Time: <span id="waitTime"> 00:10 </span>seconds!</div>
        <div style="background-color: green; margin-right: auto; width: 300px; padding: 10px;">Pose Time: <span id="poseTimer"> 00:10 </span>seconds!</div>
        <button id="collectWrongData">collect wrong pose</button>
        <button id="collectData">collect correct pose</button>
        <button id="trainData">train</button>
    </div>
    <div class="center">
    <p style="color:red; font-weight: bold;">Step:4 drag and drop saved model in dropzone area.</p>
    <form action="{{ route('cust.signup.setPoseUpload') }}" class="dropzone" method="POST" enctype="multipart/form-data">@csrf</form>
    </div>

    <form id='poseForm' action="{{ route('cust.signup.setPose') }}" method="POST">
    @csrf
    <div class="register center">
        <button type="button" id="pose" class="btn ">Submit Pose</button>
    </div>
    </form>
    </div>
</section>

<script> 
    if (!'{{Session::has('fname')}}')
    {
        alert("Please enter your name before setting pose");
        window.location.href = "{{route('cust.signup')}}";
    }

    
    document.getElementById("pose").addEventListener("click", function() {
        if(fileCount < 3){
            alert("Please train and upload the 3  model files to dropzone")
        }else{
            document.getElementById("poseForm").submit();
        }
    });
    
    var poseMode = '{{ Session::get('poseMode')}}'; 
    var str1 = '{{ Session::get('fname')}}';
    var str2 = '{{ Session::get('lname')}}';
    var poseID = str1.concat(""+str2);
    poseID = poseID.replace(/\s+/g, '');    
</script> 
@endsection