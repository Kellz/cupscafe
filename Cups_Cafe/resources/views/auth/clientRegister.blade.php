@extends('layouts.Layout')

@section('content')
<section class="main">
 	<div class="login_div" style="background-image: url('{{ asset('images/bg_3.jpg')}}'); height: 700px; padding: 30px;" >
		<div class="content-w3ls ">
        <div class="text-center icon">
			<span class="fa">Customer Signup</span>
		</div>
	<div class="container bg-Sienna m-5 mx-auto">
		<form id="form" action="{{ route('cust.signup.submit') }}" method="post">
			{{ csrf_field() }}
				<div class="field-group {{ $errors->has('fname') ? ' has-error' : '' }}">
					<div class="wthree-field">
						<input class="dictate" name="fname" id="fname" type="text" value="" placeholder="First Name"  >
					</div>
				</div>
				@if ($errors->has('fname'))
					<span class="text-theme-error help-block">
						<strong>First name is required</strong>
					</span>
				@endif
				<div class="field-group {{ $errors->has('lname') ? ' has-error' : '' }}">
					<div class="wthree-field ">
						<input class="dictate" name="lname" id="lname" type="text" value="" placeholder="Last Name"  >
					</div>
				</div>
				@if ($errors->has('lname'))
					<span class="text-theme-error help-block">
						<strong>Last name is required</strong>
					</span>
				@endif
				<div>
					<span class="text-theme"> Digital ID:</span>
					<select class="field-group digital_id" id="digital_id" onchange="selection();" name="digital_id">
						<option value="0">Password</option>
						<option value="1">Image</option>
						<option value="2">Audio</option>
					</select>
				</div>

				<div class = "selected" style="display: block" >
					<div class="field-group " >
						<div class="wthree-field ">
							<input class="dictate" name="password" type="Password" placeholder="Password" required>
						</div>
					</div>
	
					<div class="field-group" >
						<div class="wthree-field ">
							<input class="dictate" name="password_confirmation" type="Password" placeholder="re-enter password" required>
						</div>
					</div>
				</div>

				<div class = "selected" style="display: none;">
					<span class="text-theme">click button bellow to continue to the next page to validate pose</span>
				</div>

				<div class = "selected" style="display: none;">
					<span class="text-theme">speech detection input automatically starts after the instructions are stated. If your input is incorrect press SPACE key</span>
				</div>
				<div class="register center">
					<button type="button" class="login_btn btn" onclick="btnPress()">Register</button>
				</div>
			</form>
	</div>
		</div>
	</div>
</section>
 <script>
	dictationInput = "signup";
 </script>
@endsection