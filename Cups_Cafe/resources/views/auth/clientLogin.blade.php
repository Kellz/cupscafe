@extends('layouts.Layout')

@section('content')
<section class="main">

 	<div class="login_div" style="background-image: url('{{ asset('images/bg_3.jpg')}}'); height: 100vh; padding: 30px;" >
		<div class="content-w3ls ">
        <div class="text-center icon">
			<span class="fa">Customer Login</span>
		</div>
	<div class="container bg-Sienna m-5 mx-auto">	
		<form action="{{ route('cust.login.submit') }}" method="post">
			{{ csrf_field() }}
				<div class="field-group {{ $errors->has('fname') ? ' has-error' : '' }}">
					<div class="wthree-field">
						<input class="dictate" name="fname" id="fname" type="text" value="" placeholder="First Name" required>
					</div>
				</div>
				@if ($errors->has('fname'))
					<span class="text-theme-error help-block">
						<strong>{{ $errors->first('email') }}</strong>
					</span>
				@endif
				<div class="field-group {{ $errors->has('lname') ? ' has-error' : '' }}">
					<div class="wthree-field">
						<input class="dictate" name="lname" id="lname" type="text" value="" placeholder="Last Name" required>
					</div>
				</div>
				@if ($errors->has('lname'))
					<span class="text-theme-error help-block">
						<strong>{{ $errors->first('email') }}</strong>
					</span>
				@endif
				<div>
					<span class="text-theme">Digital ID:</span>
					<select class="field-group digital_id" onchange="selection();" name="digital_id">
						<option value="0">Password</option>
						<option value="1">Image</option>
						<option value="2">Audio</option>
					</select>
				</div>
				<div class="field-group selected" style="display: block {{ $errors->has('password') ? ' has-error' : '' }}">
					<div class="wthree-field">
						<input class="dictate" name="password" id="myInput" type="Password" placeholder="Password">
					</div>
				</div>
				@if ($errors->has('password'))
					<span class="text-theme-error help-block">
						<strong>{{ $errors->first('password') }}</strong>
					</span>
				@endif
				<div class = "selected" style="display: none;">
					<span class="text-theme">click button bellow to continue to the next page to validate pose</span>
				</div>

				<div class = "selected" style="display: none;">
					<span class="text-theme">speech detection input automatically starts after the instructions are stated. If your input is incorrect press SPACE key</span>
				</div>

				<div class="wthree-field">
					<button type="submit" class="login_btn btn">Login</button>
				</div>
				
			</form>
			<form action="{{ route('cust.signup') }}" method="get">
				<div class="register center">
				<button type="submit" class="btn">Register</button>
				</div>
			</form >
	</div>
		
		</div>
	</div>
</section>
  <script>
	dictationInput = "login";
 </script>
@endsection