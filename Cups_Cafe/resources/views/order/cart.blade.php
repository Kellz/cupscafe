@extends('order.orderlayout')
@section('content')
<section id="orderListSection" class="ftco-section ftco-cart">

    <div class="container">
        <div class="row" id="orderList">
            <div class="col-md-12 ftco-animate">
                <div class="cart-list">
                    <br />

                    @if(Session::has('success'))
                    <div class="alert alert-success">
                        <p>{{ Session::get('success') }}</p>
                    </div>

                    @elseif(Session::has('error'))
                    <div class="alert alert-error">
                        <p>{{ Session::get('error') }}</p>
                    </div>
                    @endif
                    <table class="table" id="cart">
                        <thead class="thead-primary" id="cart_headings">
                            <tr class="text-center">
                                <th>&nbsp;</th>
                                <th>Product</th>
                                <th>Price</th>
                                <th>Quantity</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if ($items->isEmpty())
                            <h1 class="col p-3 text-center ftco-animate">No Items in cart</h1>

                            @else
                            @foreach($items as $item)
                            <tr class="text-center" id="items">

                                <td class="product-remove">
                                    <form action="{{route('order.remove')}}" method="post">
                                        @csrf
                                        <input type="hidden" value="{{$item->rowId}}" name="id">
                                        <button type="submit" class=" btn  py-3 px-4 icon-close"></button>
                                    </form>
                                </td>

                                <td class="item_name">
                                    <h3>{{$item->name}}</h3>
                                </td>

                                <td class="price">{{$item->price}}</td>

                                <td class="quantity">{{$item->qty}}</td>

                                <td class="total">${{$item->subtotal}}</td>

                            </tr>
                            @endforeach

                            <tr>
                                <td>
                                    <form action="{{route('order.destroy')}}" method="post">
                                        @csrf
                                        <button type="submit" class=" btn py-3 px-4">Empty</button>
                                    </form>
                                </td>

                                <td>                            

                                    <button id="myBtn" type="button" onclick="myFunction()" class=" btn py-3 px-4">Confirm</button>

                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        @endif 
        <div id="confirmUser" style="background-image: url('{{ asset('images/bg_3.jpg')}}');  display: none; padding: 30px;">
            <div class="login_div" >
              <div class="text-center icon">
              <span class="fa">Customer Login</span>
            </div>
            <form action="{{route('order.confirm')}}" method="post">
            {{ csrf_field() }}
              <div class="field-group ">
                <div class="wthree-field">
                  <input class="dictate" name="fname" id="fname" type="text" value="" placeholder="First Name" required>
                </div>
                  </div>

              <div class="field-group ">
                <div class="wthree-field">
                  <input class="dictate" name="lname" id="lname" type="text" value="" placeholder="Last Name" required>
                </div>
              </div>

              <div>
                <span class="text-theme">Digital ID:</span>
                <select class="field-group digital_id" onchange="selection();" name="digital_id">
                  <option value="0">Password</option>
                  <option value="1">Image</option>
                  <option value="2">Audio</option>
                </select>
              </div>
              <div class="field-group selected" style="display: block">
                <div class="wthree-field">
                  <input class="dictate" name="password" id="myInput" type="Password" placeholder="Password">
                </div>
              </div>

              <div class = "selected" style="display: none;">
                <div class="center" id="sketch" style="display: block; width:600px; margin: auto; padding: 60px 0px 5px 0px;">
                </div>
                <span class="text-theme">click button bellow to continue to the next page to validate pose</span>
              </div>

              <div class = "selected" style="display: none;">
                
                <span class="text-theme">speech detection input automatically starts after the instructions are stated. If your input is incorrect press SPACE key</span>
              </div>

              <div class="wthree-field ">
                <button type="submit" class="login_btn btn bg-dark">Login</button>
              </div>
              <div class="wthree-field ">
                <button type="submit" style="width: 100px; color:black;" onclick="myFunction()" class="login_btn btn bg-red">cancel</button>
              </div>
              @if ( isset ( $val ) )
               <input type="hidden" name="attempts" value="1">
              @elseif( !isset($val) )
              <input type="hidden" name="attempts" value="{{ Session::get('attempts')}}">
             
              @endif
            </form>
          </div>
        </div>
      </div>
    </div>
</section>





<style>
body {font-family: Arial, Helvetica, sans-serif;}

.close:hover,
.close:focus {
  color: #000;
  text-decoration: none;
  cursor: pointer;
}
</style>
<script>

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

function myFunction() {
  var x = document.getElementById("confirmUser");
  var y = document.getElementById("orderList");
  if (x.style.display === "none") {
    x.style.display = "block";
    y.style.display = "none";
  }else{
    x.style.display = "none";
    y.style.display = "block";
  }
}
    dictationInput = "login";
    confirmation = true;
 </script>
@endsection