@extends('order.orderlayout')

@section('content')

<section class="main">
    <script src="{{ asset('functionality/pose.js') }}"></script>
    <div class="login_div" style="background-image: url('{{ asset('images/bg_3.jpg')}}'); height: 1100px; padding: 30px;" >
    
    <div class="center" id="sketch" style="display: block; width:600px; margin: auto; padding: 60px 0px 5px 0px;">
    </div>

    <form id = "poseLoginForm" action="{{ route('order.confirm') }}" method="POST">
    @csrf
    <input type="hidden" name="fname" value="{{ Session::get('cfname')}}">
    <input type="hidden" name="lname" value="{{ Session::get('clname')}}">
    <input type="hidden" name="digital_id" value="0">
    <input type="hidden" id="password" name="password" value="">
    </form>
    </div>
</section>
<script> 


    if (!'{{Session::has('fname')}}')
    {
        alert("Please enter your name before setting pose");
        window.location.href = "{{route('order.cart')}}";
    }

    var str1 = '{{ Session::get('cfname')}}';
    var str2 = '{{ Session::get('clname')}}';
    var poseMode = "get"; 
    var poseID = str1.concat(""+str2);
    poseID = poseID.replace(/\s+/g, '');    
    var main = '{{route('order.cart')}}';
    if(str1 == '{{ Session::get('fname')}}' && str2 == '{{ Session::get('lname')}}'){
        var public_path1 = '/model/'+poseID+'.json';
        var public_path2 = '/model/'+poseID+'_meta.json';
        var public_path3 = '/model/'+poseID+'.weights.bin';
    }else{
        window.location.href = main;
    }

    
    document.getElementById("password").value = poseID;
    setTimeout(function () {
            console.log('pose not found');
            alert('sorry your pose was not found or the page timed out please try again');
            window.location.href = main;
        }, 30000);
   
</script> 
@endsection










