@extends('order.orderlayout')
@section('content')

<section class="ftco-section contact-section">
    <div class="container bg-dark m-5 mx-auto">
        @if(Session::has('success'))
        <div class="alert alert-success">
            <p>{{ Session::get('success') }}</p>
        </div>

        @elseif(Session::has('error'))
        <div class="alert alert-error">
            <p>{{ Session::get('error') }}</p>
        </div>
        @endif

        <div class="row">

            @if ($menus->isEmpty())
            <br />
            <div class="col p-3 text-center ftco-animate">
                <h1 class="text-center"> No Items On Menu</h1>
            </div>

            @else
            @foreach($menus as $menu)
            <div class="col-md-4 col p-5 ftco-animate">
                <div class="menu-entry">
                    <img class="menu-img img mb-4 col-md-12" id="display"
                        src="../storage/item_photo/{{$menu->item_photo}}"
                        onmouseover="src='../storage/asl_item/{{$menu->asl_item}}'"
                        onmouseout="src='../storage/item_photo/{{$menu->item_photo}}'">
                    <audio controls class="col-md-12">
                        <source src="../storage/item_audio/{{$menu->item_audio}}" type="audio/mp3">
                        </source>
                    </audio>
                    <div class="text text-center pt-4">
                        <h3>{{$menu->name}}</h3>
                        <p class="price"><span>${{$menu->cost}}</span></p>
                        <form action="{{route('order.store',$menu)}}" method="post">
                            @csrf
                            <input type="hidden" value="{{$menu->item_id}}" name="id">
                            <input type="hidden" value="{{$menu->name}}" name="item_name">
                            <input type="hidden" value="{{$menu->cost}}" name="cost">
                            <div class="quantity col-md-12">
                            <input type="number" value="1" name="qty" class="input" min="1" max="50" required>
                             </div>
                            <button type="submit" class="btn btn-primary ">+ Cart</button>
                           
                        </form>
                    </div>
                </div>
            </div>
            @endforeach
            @endif
        </div>
    </div>
    </div>
</section>

@endsection