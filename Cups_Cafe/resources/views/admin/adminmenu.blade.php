@extends('admin.adminlayout')
@section('content')


<section class="ftco-section ftco-services">
    <div class="container bg-brown m-5 mx-auto">
        <div class="row">
            @if ($menus->isEmpty())
            <br />
            <div class="col p-3 text-center ftco-animate">
                <h1 class="text-center"> No Items Added</h1>
            </div>

            @else
            @foreach($menus as $menu)
            <div class="col-md-4 col p-5 ftco-animate">
                <div class="menu-wrap">
                    <img class="menu-img img mb-4 col-md-12" src="storage/item_photo/{{$menu->item_photo}}" />
                    <div class="overlay">
                        <img class="menu-img img mb-4 col-md-12" src="storage/asl_item/{{$menu->asl_item}}" />
                    </div>
                    <audio controls class="col-md-12">
                        <source src="../storage/item_audio/{{$menu->item_audio}}" type="audio/mp3">
                        </source>
                    </audio>
                    <div class="text text-center pt-4">
                        <h3>{{$menu->name}}</a></h3>
                        <p>Quantity: {{$menu->unit_quantity}}</p>
                        <p class="price"><span>${{$menu->cost}}</span></p>
                    </div>
                    <div class="row">
                        <div class="col-6">
                                <input class="btn btn-success text-white btn-block" role="button" type="submit"
                                    value="Update" onclick="openForm()">
                                    <div class="form-popup" id="myForm">
                                        <form action="{{route('admin.update',$menu->item_id)}}" method="post" class="form-container">
                                            {{ csrf_field() }}  
                                            <div class="col-md-12 ">
                                                <input type="hidden" value="{{$menu->item_id}}" name="item_id" required>

                                                <input type="number" value="" placeholder="Enter Quantity" name="unit_quantity" required>
                                            </div>
                                            <div class="col-md-12">
                                                <input type="number" value="" placeholder="Enter Price" name="cost" required>
                                            </div>                                 
                                             <input class="btn btn-success text-white btn-block" role="button" type="submit" value="Update Stock">                                  
                                           
                                            </br>
                                            <input class="btn btn-danger text-white btn-block" role="button" type="submit" value="Close" onclick="closeForm()">                                       
                                        </form>
                                    </div>
                                    <script>
                                        function openForm() {
                                        document.getElementById("myForm").style.display = "block";
                                        }

                                        function closeForm() {
                                        document.getElementById("myForm").style.display = "none";
                                        }
                                    </script>
                            </form>
                        </div>
                        <div class="col-6">
                            <form action="{{route('admin.destroy',$menu->item_id)}}" method="post" role="item"
                                enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input class="btn btn-danger text-white btn-block" role="button" type="submit"
                                    value="Delete">
                            </form>
                        </div>

                    </div>
                </div>

            </div>
            @endforeach
            @endif
        </div>

    </div>
</section>

@endsection