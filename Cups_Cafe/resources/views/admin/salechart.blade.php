@extends('admin.adminlayout')

@section('content')
<section class="ftco-section contact-section">
    <div class="container">
		<div class="row slider-text justify-content-center align-items-center" data-scrollax-parent="true">
            <div class="col-md-8 col-sm-12 col p-3 text-center ftco-animate">
                    <h1>Sales Chart</h1>           
            </div>  
        </div> 
    </div> 
    <div class="container bg-white align-items-center">
     
        {!!$salesChart->container()!!}
       
    </div>
</section>
...
    {{-- ChartScript --}}
    @if($salesChart)
    {!! $salesChart->script() !!}
    @endif



@endsection