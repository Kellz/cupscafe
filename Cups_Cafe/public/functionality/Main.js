var dictationInput;
var confirmation = false;

function selection() {
    var i = 0;
    var myDiv = document.getElementsByClassName('selected');
    var div;
    while (i < myDiv.length) {
        myDiv[i++].style.display = 'none';
    }

    var v = $("select.digital_id").children("option:selected").val();
    if (v == 0) {
        dictation(v);
        document.getElementsByClassName("login_btn")[0].innerHTML = "Submit";
    } else if (v == 1) {
        dictation(v);
        document.getElementsByClassName("login_btn")[0].innerHTML = "Pose";
    } else {
        dictation(v);
        document.getElementsByClassName("login_btn")[0].innerHTML = "Submit Dictation";
    }

    myDiv[v].style.display = 'block';
}



function btnPress() {
    document.forms["form"].submit();
}





function dictation(v) {
    //recognition.interimResults = true;
    try {
        var SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition;
        var recognition = new SpeechRecognition();
    } catch (e) {
        console.error(e);
        $('.no-browser-support').show();
        $('.app').hide();
    }
    var i = 0;
    let speechInput = document.getElementsByClassName('dictate');
    recognition.addEventListener('result', e => {
        console.log(e.results);
        const transcript = Array.from(e.results)
            .map(result => result[0])
            .map(result => result.transcript)
            .join('')

        if (e.results[0].isFinal) {
            if (dictationInput == "signup") {
                var res = transcript.split(" ");
                speechInput[0].value = res[0];
                speechInput[1].value = res[1];
                speechInput[2].value = res[2];
                speechInput[3].value = res[2];
            } else if (dictationInput == "login") {
                var res = transcript.split(" ");
                speechInput[0].value = res[0];
                speechInput[1].value = res[1];
                speechInput[2].value = res[2];
            }
            readOutLoud("your first name is " + speechInput[0].value + ", last name " + speechInput[1].value + "password" + speechInput[2].value);
        }
        console.log(transcript);
    });
    event(v);

    function event(v) {
        if (v < 2) {
            recognition.stop();
        } else {
            readOutLoud("After recording, please state your first name, last name and password? Please say it in one go, example rohan bedward 123. Press Space Bar to redo");
            setTimeout(function () {
                recognition.start();
            }, 14000);
        }
    }


    const log = document.getElementById('log');

    document.addEventListener('keypress', logKey);

    function logKey(e) {
        console.log(e.code);
        if (e.code == 'Space') {
            event(v);
        }
    }
    recognition.onstart = function () {
        console.log('Voice recognition activated. Try speaking into the microphone.');
    }

    recognition.onspeechend = function () {
        console.log('You were quiet for a while so voice recognition turned itself off.');

    }

    recognition.onerror = function (event) {
        if (event.error == 'no-speech') {
            console.log('No speech was detected. Try again.');
        };
    }



    function readOutLoud(message) {
        var speech = new SpeechSynthesisUtterance();

        // Set the text and voice attributes.
        speech.text = message;
        speech.volume = 1;
        speech.rate = 1;
        speech.pitch = 1;

        window.speechSynthesis.speak(speech);
    }
}
