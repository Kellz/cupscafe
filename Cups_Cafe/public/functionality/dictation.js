window.SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition;

const recognition = new SpeechRecognition();
recognition.interimResults = true;
let i = 0;
let input = document.getElementsByClassName('dictate')[i];



recognition.addEventListener('results', e => {
    const transcript = Array.from(e.results)
        .map(result => result[0])
        .map(result => result.transcript)
        .join('')

    input[i].textContent = transcript;
    if (e.results[0].isFinal) {
        words.appendChild(input[i]);
        i++;
    }
    console.log(transcript);
});

if (i == 4) {
    document.forms["form"].submit();
} else {
    recognition.addEventListener('end', recognition.start);
    recognition.start();
}
