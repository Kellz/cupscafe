let trained = false;
let collecting = false;
let video;
let poseNet;
let pose;
let skeleton;
let brain;
let state = 'waiting';
let targeLabel = 'pose';
let poselist
let count = 0;
let poseLabel = "Do your pose";

function setup() {
    canvas = createCanvas(640, 480);
    canvas.parent('sketch');
    video = createCapture(VIDEO);
    video.hide();
    poseNet = ml5.poseNet(video, modelLoaded);
    poseNet.on('pose', gotPoses);
    let options = {
        inputs: 34,
        outputs: 2,
        task: 'classification',
        debug: true
    }

    brain = ml5.neuralNetwork(options);
    // Buttons
    if (poseMode == "set") {
        select('#collectWrongData').mousePressed(wrong);
        select('#collectData').mousePressed(correct);
        select('#trainData').mousePressed(trainModel);

    } else if (poseMode == "get") {

        const modelInfo = {
            model: public_path1,
            metadata: public_path2,
            weights: public_path3,
        };
        brain.load(modelInfo, brainLoaded);
    }
}

function wrong() {
    doPose('F');
}

function correct() {
    doPose('P');
}
//login Mode
function brainLoaded() {
    console.log('pose classification ready!');
    classifyPose();
}

function classifyPose() {
    if (pose) {
        let inputs = [];
        for (let i = 0; i < pose.keypoints.length; i++) {
            let x = pose.keypoints[i].position.x;
            let y = pose.keypoints[i].position.y;
            inputs.push(x);
            inputs.push(y);
        }
        brain.classify(inputs, gotResult);
    } else {
        setTimeout(classifyPose, 100);
    }
}

function gotResult(error, results) {

    if (results[0].confidence > 0.75) {
        poseLabel = results[0].label.toUpperCase();
    }
    //console.log(results[0].confidence);
    classifyPose();
}



//signup Mode
function gotPoses(poses) {
    // console.log(poses); 
    if (poses.length > 0) {
        pose = poses[0].pose;
        skeleton = poses[0].skeleton;
        if (state == 'collecting') {
            let inputs = [];
            for (let i = 0; i < pose.keypoints.length; i++) {
                let x = pose.keypoints[i].position.x;
                let y = pose.keypoints[i].position.y;
                inputs.push(x);
                inputs.push(y);
            }
            let target = [targetLabel];
            brain.addData(inputs, target);
        }
    }
}


function doPose(type) {
    targetLabel = type;
    console.log(targetLabel);
    timer('waitTime');
    setTimeout(function () {
        timer('poseTimer');
        console.log('collecting');
        state = 'collecting';
        setTimeout(function () {
            console.log('not collecting');
            state = 'waiting';
        }, 10000);
    }, 10000);

}

function trainModel() {
    // No longer collecting dataa
    brain.normalizeData();
    let options = {
        epochs: 50
    }
    brain.train(options, finishedTraining);
}

function finishedTraining() {
    console.log('done');
    brain.save();
}

//timer 
function startTimer(duration, display) {
    var timer = duration,
        minutes, seconds;
    var idTime = setInterval(function () {
        minutes = parseInt(timer / 60, 10)
        seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        display.textContent = minutes + ":" + seconds;

        if (timer == 0) {
            clearInterval(idTime);
        }

        if (--timer < 0) {
            timer = duration;
        }
    }, 1000);
}

function timer(t) {
    var fiveMinutes = 10,
        display = document.querySelector('#' + t);
    startTimer(fiveMinutes, display);
}

function modelLoaded() {
    console.log('poseNet ready');

    /*
    if (poseMode == "set" && count == 0) {
        count++;
        setup();
    }
    */
}

//skeleton display
function draw() {
    push();
    translate(video.width, 0);
    scale(-1, 1);
    image(video, 0, 0, video.width, video.height);

    if (pose) {
        for (let i = 0; i < skeleton.length; i++) {
            let a = skeleton[i][0];
            let b = skeleton[i][1];
            strokeWeight(2);
            stroke(0);

            line(a.position.x, a.position.y, b.position.x, b.position.y);
        }
        for (let i = 0; i < pose.keypoints.length; i++) {
            let x = pose.keypoints[i].position.x;
            let y = pose.keypoints[i].position.y;
            fill(0);
            stroke(255);
            ellipse(x, y, 16, 16);
        }
    }
    pop();

    if (poseMode == "get") {

        fill(255, 0, 255);
        noStroke();
        textSize(30);
        textAlign(CENTER, CENTER);
        text(poseLabel, width / 2, height / 2);

        if (poseLabel == 'P') {
            console.log(poseLabel);
            $('form').submit();
            $(script[src = "functionality/pose.js"]).remove();
            $('#sketch').remove();
            // document.getElementById("poseLoginForm").submit();
        }
    }

}
